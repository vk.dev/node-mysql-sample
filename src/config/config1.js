/* eslint-disable quote-props */
import { config } from 'dotenv';

config();

module.exports = {
  development: {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "mysqldba",
    DB: "testdb",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  },
  test: {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "mysqldba",
    DB: "testdb",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  },
  production: {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "123456",
    DB: "testdb",
    dialect: "mysql",
    logging: false,
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
// module.exports = {
//   development: {
//     use_env_variable: 'DATABASE_URL_DEV',
//     dialect: 'postgres',
//     logging: false,
//   },
//   test: {
//     use_env_variable: 'DATABASE_URL_TEST',
//     dialect: 'postgres',
//     logging: false,
//   },
//   production: {
//     use_env_variable: 'DATABASE_URL_PROD',
//     dialect: 'postgres',
//     logging: false,
//     dialectOptions: {
//       ssl: {
//         require: true,
//         rejectUnauthorized: false,
//       },
//     },
//   },
// };
