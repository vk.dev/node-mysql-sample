export default {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
        unique: true,
      },
      password: {
        type: Sequelize.STRING
      },
      profile_image: {
        type: Sequelize.STRING
      },
      birth_date: {
        type: Sequelize.DATE
      },
      otp: {
        type: Sequelize.STRING
      },
      otp_date_time: {
        type: Sequelize.DATE
      },
      gender: {
        type: Sequelize.ENUM,
        values: ['male', 'female', 'other']
      },
      status: {
        type: Sequelize.ENUM,
        values: ['inactive', 'active', 'suspended'],
        defaultValue: 'inactive',
      },
      settings: Sequelize.JSON,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};
